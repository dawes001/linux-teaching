# Filesystem command Cheat Sheet

## Linux Filesystem commands

Command | Explanation | Space for your own notes
--- | --- | ---
pwd | Print the working directory | 
ls | List files in current working directory | 
cd \<path> | Change current working directory |
mkdir \<path> | Make a new directory |
rmdir \<path> | Remove an empty directory |

## Linux file commands

Command | Explanation | Space for your own notes
--- | --- | ---
touch \<file> | Create a new empty file |
cat \<file> | Print a file to STDOUT |
rm \<file> | Delete a file |
rm -r \<path> | Delete a directory recursively (DANGEROUS!) |
head \<file> | Show the first 10 lines of \<file> |
tail \<file> | Show the last 10 lines of \<file> |
nano \<file> | Open \<file> using nano |

## Nano commands
Command | Explanation | Space for your own notes
--- | --- | ---
Ctrl-X | Exit editor |
Ctrl-O | Save file (and keep editing) |
Ctrl-K | Cut line |
Ctrl-U | Paste line |
Alt-Shift-4 | Wrap text to window size |
Alt-/ | Go to end of document |
Alt-\\ | Go to start of document |


## Special paths

Name | details | Space for your own notes
--- | --- | ---
/ | Root - core of the filesystem | 
. | This working directory | 
.. | The parent directory | 
~ | Your home directory | 

