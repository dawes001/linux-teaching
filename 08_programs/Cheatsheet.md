# Linux Command Cheat Sheet

## More Linux commands

Command | Explanation | Space for your own notes
--- | --- | ---
echo \<text> | Prints \<text> to STDOUT | 
cat \<file> | Print a file to STDOUT |
fortune | Prints a random fortune to STDOUT |
cowsay | Makes a cow say STDIN |
wc | Counts the words, lines and characters of STDIN |
mail \<recipient> | Sends an email containing STDIN to a specific recipient |
man \<command> | Gets help on \<command> |
env | Prints your current environment variables |
history | Prints your current command history |

## Special symbols

Name | Details | Space for your own notes
--- | --- | ---
\<command1> &#124; \<command2> | Pipe the STDOUT of one command to the STDIN of another |
\<command> \> \<file> | Redirect the STDOUT of a command into a file |
\<command> \>\> \<file> | Append the STDOUT of a command into a file |
"\<text>" | Regular quotes - all text contained is one argument | 
'\<text>' | Literal quotes - no variable expansion | 
\`\<command>\` | Backticks - execute commands in subshell | 
`` | Backticks - execute commands in subshell |  
$\<text> | expand environment variable on execution | 
$() | (BASH) Better backticks | 

## Common environment variables

Name | Details | Space for your own notes
--- | --- | ---
$USER | Your current username | 
$PATH | Your current command path | 
$HOST | The current name of the machine | 
$HOME | Your current home directory | 
$PWD | Your current working directory | 
