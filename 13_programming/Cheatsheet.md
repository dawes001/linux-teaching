# Bash Programming Cheat Sheet

## Special Variables
Name | Details | Space for your own notes
--- | --- | ---
\#! | A shebang - indicating which process to interpret this script with | 
$? | Return code of the last command | 
$1 | First argument | 
$2 | Second argument | 
$3-99 | Third to ninety ninth argument | 
$0 | The name of the command given to execute this script | 

## Bash Programming statements

Statement | Explanation | Space for your own notes
--- | --- | ---
\<command1> ; \<command2> | Do \<command1>, then \<command2> independent of how it returns | 
\<command1> && \<command2> | Do \<command1>, then \<command2> if \<command1> succeeds | 
\<command1> &#124;&#124; \<command2> | Do \<command1>, then \<command2> if \<command1> fails | 
for x in a b c ; do \<command> $x ; done | Loop over the contents of this list | 
if \<command1> ; then \<command> $x ; fi | If \<command1> doesn't return 0, do \<command2> | 
while \<command1> ; do \<command2> ; done | If \<command1> doesn't return 0, do \<command2> until it does | 

## Even more bash commands

Command | Explanation | Space for your own notes
--- | --- | ---
test \<statement> | Returns 0 if a statement is true, 1 otherwise | 
\[ \<statement> ] | synomym for test, above | 
! \<command> | If \<command> returns 0; return 1; otherwise return 0 | 
basename \<path> | The name of the file written in path, without the directory | 
dirname \<path> | The directory written in path, without the file | 
