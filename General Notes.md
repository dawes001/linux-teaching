* Welcome (5-10 mins)
 - Get a feel for people's level
 - Allow people to self-indicate experience - rearrange room so experienced people sit near novices

* Intro (15-20 mins, depending on skill)
 - Make sure people can access the GRD documents
 - Have a few (20%?) printed copies
 - Expect that most people will be able to reach PDFs
 - Consider a shortened URL?
 - Make sure everyone can log in (PuTTY)
 - Talk about WinSCP?
 
* Explain about the shell (Chapter 3, GRD1) (15-20 mins)
 - REPL concept
 - BASH vs SH
 - Ask if Bashisms are too distracting at first - can use sh for first demos
 
* Distribute cheatsheets for Filesystems
* Short intro and on-beamer demonstration of filesystem handling (20 mins)
 - Root
 - Paths
 - Dot/Dotdot
* Files
 - touch/rm
 - editors(nano/vi)

 - Do some hands on stuff (GRD1 Chapter 6) (may have to write this) (10-20 mins)

* Q&A point - Good speed?
 - Check for time - accurate? (Critical for HPC course)
 
* Distribute cheatsheets for Programs
* Linux program philosophy (30 mins)
 - Each program does one thing and does it well
 - The pipe
 - Stdout/stderr redirection

 - Do some hands on stuff (GRD2 Chapter 8) (30 mins)

* Distribute cheatsheets for Programming
* Writing bash scripts (~30 mins)
 - The shebang
 - Variables and arguments

 - Do some hands on stuff (GRD2 Chapter 3) (This is complex and will probably involve more back-and-forth work) (30 mins)

* Distribute cheatsheets for Git
* Source code management (20 mins)
 - git

 - Do some hands on stuff (Not in GRD!)

* Other cool stuff / Q&A
